# Javascript 30
**Applications in this repository, developed by the [JavaScript30](https://javascript30.com/) pieces of training that offered by [Wes Bos](https://wesbos.com).**

![bg](https://user-images.githubusercontent.com/25087769/81427529-98d35d00-9163-11ea-883b-e8dae7d036b5.png)


<hr>

There are live pages to see this training.

If you want to see the example applications in the course, please click on the relevant subject.

![subject](https://user-images.githubusercontent.com/25087769/81423306-f912d080-915c-11ea-8e57-4ea861556038.png)

[![01](https://user-images.githubusercontent.com/25087769/81423312-f9ab6700-915c-11ea-8b57-08f449b7702c.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/01_JavaScriptDrumKit/)
[![02](https://user-images.githubusercontent.com/25087769/81423314-fadc9400-915c-11ea-8155-bf7872d4e7c2.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/02_JS_and_CSS_Clock/)
[![03](https://user-images.githubusercontent.com/25087769/81423317-fb752a80-915c-11ea-8d24-ba324beece08.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/03_CSSVariables/)
[![04](https://user-images.githubusercontent.com/25087769/81423320-fb752a80-915c-11ea-85ac-89366eceb763.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/04_ArrayCardioDay1/)
[![05](https://user-images.githubusercontent.com/25087769/81423321-fc0dc100-915c-11ea-8726-d58307e00ca5.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/05_FlexPanelGallery/)
[![06](https://user-images.githubusercontent.com/25087769/81423325-fc0dc100-915c-11ea-8fa1-b93295930939.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/06_TypeAhead/)
[![07](https://user-images.githubusercontent.com/25087769/81423326-fca65780-915c-11ea-98d9-85923cab7730.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/07_ArrayCardioDay2/)
[![08](https://user-images.githubusercontent.com/25087769/81423328-fca65780-915c-11ea-892f-fcb153b8b515.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/08_FunwithHTML5Canvas/)
[![09](https://user-images.githubusercontent.com/25087769/81423329-fd3eee00-915c-11ea-9715-7ca91ead022e.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/09_DevToolsDomination/)
[![10](https://user-images.githubusercontent.com/25087769/81423330-fd3eee00-915c-11ea-87ce-751c56d6dc89.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/10_HoldShiftandCheckCheckboxes/)
[![11](https://user-images.githubusercontent.com/25087769/81423333-fdd78480-915c-11ea-971f-f6281ce44151.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/11_CustomVideoPlayer/)
[![12](https://user-images.githubusercontent.com/25087769/81423334-fdd78480-915c-11ea-8ae1-95b0dbf6b93e.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/12_KeySequenceDetection/)
[![13](https://user-images.githubusercontent.com/25087769/81423337-fe701b00-915c-11ea-86bb-ef6a75924576.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/13_SlideInOnScroll/)
[![14](https://user-images.githubusercontent.com/25087769/81423338-ff08b180-915c-11ea-9cdb-26c761403be8.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/14_JavaScriptReferencesVSCopying/)
[![15](https://user-images.githubusercontent.com/25087769/81423339-ff08b180-915c-11ea-97c8-090fdf2293ad.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/15_LocalStorage/)
[![16](https://user-images.githubusercontent.com/25087769/81423342-ffa14800-915c-11ea-8364-9911309beaf2.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/16_MouseMoveShadow/)
[![17](https://user-images.githubusercontent.com/25087769/81423344-ffa14800-915c-11ea-8c1f-3edba5376699.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/17_SortWithoutArticles/)
[![18](https://user-images.githubusercontent.com/25087769/81423346-0039de80-915d-11ea-91c4-df037978ead1.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/18_AddingUpTimeswithReduce/)
[![19](https://user-images.githubusercontent.com/25087769/81423348-0039de80-915d-11ea-8341-a4069df4e096.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/19_WebcamFun/)
[![20](https://user-images.githubusercontent.com/25087769/81423350-00d27500-915d-11ea-9906-7d95c69a72f9.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/20_Speech_Detection/)
[![21](https://user-images.githubusercontent.com/25087769/81423351-00d27500-915d-11ea-9ba0-84fa7eaa6c07.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/21_GeoLocation/)
[![22](https://user-images.githubusercontent.com/25087769/81423275-f1ebc280-915c-11ea-8eb1-f83ed5688e75.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/22_FollowAlongLinkHighlighter/)
[![23](https://user-images.githubusercontent.com/25087769/81423281-f31cef80-915c-11ea-8e5c-b71a553a6b3c.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/23_SpeechSynthesis/)
[![24](https://user-images.githubusercontent.com/25087769/81423285-f44e1c80-915c-11ea-91b5-14cc7b7ab80f.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/24_StickyNav/)
[![25](https://user-images.githubusercontent.com/25087769/81423288-f4e6b300-915c-11ea-8d95-f16e92e799ea.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/25_EventCapture_Propagation_BubblingAndOnce/)
[![26](https://user-images.githubusercontent.com/25087769/81423289-f57f4980-915c-11ea-964a-8685c27c3ded.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/26_StripeFollowAlongNav/)
[![27](https://user-images.githubusercontent.com/25087769/81423291-f6b07680-915c-11ea-8714-2166804ec520.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/27_Click_and_Drag/)
[![28](https://user-images.githubusercontent.com/25087769/81423296-f7490d00-915c-11ea-91db-a3dc6ba08169.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/28_VideoSpeedController/)
[![29](https://user-images.githubusercontent.com/25087769/81423302-f7e1a380-915c-11ea-833b-e4b33d4bcd2c.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/29_CountdownTimer/)
[![30](https://user-images.githubusercontent.com/25087769/81423305-f87a3a00-915c-11ea-8e1f-f6f6e44961b6.png)](https://mustafadalga.github.io/Front-End-Developments/Javascript30/30_Whack_A_Mole/)










